## Project Name: ESP32-C3 1-Port RGB Controller

This KiCAD 6 designed PCB will allow simple control of a string of up to 25 WS2812 RGB LED's or more, if the RGB LED string is powered externally and Q3 is turned off to isolate the board from external 5V power.  The onboard phototransitor will provide a rough indicator of ambient light levels.  The Red and Green LEDs and the two tactile switches allow for programmatic control and feedback etc.

## Features:
- ESP32-C3-MINI MCU
- small 35mm x 26mm dual layer PCB, so much cheaper to panelise than a four layer PCB
- USB-C up to 3A to power a small segment of RGB LEDs and controller
- ambient light sensor (Q1)
- proper 3V3 to 5V logic level translator IC
- power on/off switches
- MOSFET controlled onboard power of the RGB LED string. This can isolate externally powered LED string from board power. Allows board powered LED string to be turned off
- Mode and Boot tactile switches can both be programmaticly used
- on board Red and Green LEDs for programmatic indications
- USB ESD 3A fuse and ESD protection
- Access  to ESP32-C3 serial port for external UART program flashing or data logging/testing etc
- upload programs directly over USB port to native ESP32-C3 data gpio

