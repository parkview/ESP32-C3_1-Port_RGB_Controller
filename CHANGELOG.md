## ESP32-C3 1-Port RGB Controller PCB Changelog

### Version 2.1: 1-Port 
PCB Details:  JLCPCB, 2 layer, Purple, 1.6mm, not panelised  
Production Date: 2023-01-07  
 
- Done: change the rest of 10K resistors: R3, R14, R19 to 0402 
- Done: change R9: 4.7K/5.6K to 0402 
- Done: make front ON/OFF text font fatter 
- Done: remove JLCPCB batch number 
- Done: add 10K pullup to GPIO8 - strapping needed 
- Done: change Q3 to a dual MOSFET to 100% isolate LED strings 5V from boards 5V 
 
 
### Version 2.0: 1-Port 
PCB Details:  JLCPCB, 2 layer, green, 1.2mm, not panelised  
Production Date: 2022-09-09  
 
- based off of the 2-Port version 1.0 PCB design 
- design goal is to make it as small as possible: 35mm x 26mm 
- Done: ditch the Haptic circuit 
- Done: ditch 2nd RGB output 
- Done: ditch the OLED connector 
- Done: move R5+R6 to 0402 and place on RH side of USB connector 
- Done: update backside with correct GPIO:  Light 
- Done: move board title elsewhere and move RGB port labels down closer to board edge 
- Done: fix Mode GPIO label that's over a via 
- Done: add a small 2A MOSFET to switch RGB 5V line off/on - as a Reset 
 
